<?php
class Database{

    //Variáveis de Conexão
    private $host = "localhost";
    private $dbName = "assessment_backend_xp";
    private $username = "root";
    private $password = "";

    public $conn;

    //função para retornar conexão segura
    public function dbConnection(){
        $this->$conn = null;
        try{
            $this->$conn = new PDO("mysql:host=".$this->host.";dbname=".$this->dbName,$this->username,$this->password,array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            ));
        }catch(PDOException $exception){
            echo "Connection error: ". $exception->getMessage();
        }
        return $this->conn;
    }
}
?>